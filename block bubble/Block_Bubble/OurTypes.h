#ifndef OURTYPES_H
#define OURTYPES_H

#include "ebentoak.h"

typedef enum { JOLASTEN, GALDU, IRABAZI }EGOERA;
typedef enum { EASY, MEDIUM, HARD, APOCALYPTIC }ZAILTASUNA;
typedef enum { IRUDIA, MARGOA, TESTUA } MOTA;


typedef struct sagua
{
	//sistemak ematen digun datuak
	POSIZIOA pos;

	float mousepx;
	float mousepy;
	int mousex;
	int mousey;

}SAGUA;

typedef struct timer
{
	//katanarako erabiltzen dira //Uint32  current_time = SDL_GetTicks();
	int id;
	POSIZIOA pos;
	int denboraHasiera;
	int denboraBukaera;
	int tartea;

}TIMER;

typedef struct ranking
{
	int posizioa;
	int zonbie_kills;
	char jokalariaren_izena[128];

}RANKING;


typedef struct
{
	POSIZIOA pos;
	int id;
	MOTA mota;

	int dirua;
	int babel_kills;

	int tiroa;
	float tiro_angelua;

	int kont;//////////////////////////

	int pantailan_kopurua;

}JOKALARIA;

typedef struct
{
	POSIZIOA pos;
	int id;

	int movx;
	int movy;

	int beharrezko_killak;
	int sortuta;
	int jasotako_tiroak;
	int hilda;

}BOSS;


typedef struct
{
	POSIZIOA pos;
	int id;

	int bizirik_al_dago;
	int generatu_kont;

}ETSAIA;

typedef struct
{
	POSIZIOA pos;
	int id;
	int generatu_kont;
	int kantidadea;

}BALA;

typedef struct
{
	int easy;
	int medium;
	int hard;
	int apocaliptic;
	int kont;
	int erratuak;
	int modo;
	//int pantailan_kopurua;

}MAILA;

#endif