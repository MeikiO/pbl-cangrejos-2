
#ifndef INCLUDES_H
#define INCLUDES_H

		//---LAS LIBRERIAS---
#include <stdio.h>
#include <windows.h>
#include "string.h"

#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"

#include <SDL.h>
#include <SDL_joystick.h>		 //--> para ponerle mandos de xbox
#include <SDL_video.h>		//--> para a�adir video.
#include <SDL_thread.h>  //
#include <SDL_timer.h>  //
#include <SDL_render.h>
#include <SDL_ttf.h>


	//---JOKOAN ZEHAR SORTUTAKO ELEMENTUEN LIBURUTEGIAK---

	#include "jokalaria.h"
	#include "enemigos.h"
	#include "mailen_kudeaketa.h"

		//---IRUDIAK---
#define EASY_FONDO ".\\img\\easy.bmp"
#define MEDIUM_FONDO ".\\img\\medium.bmp"
#define HARD_FONDO ".\\img\\hard.bmp"
#define APOCALYPTIC_FONDO ".\\img\\apocalyptic.bmp"

#define JOKOA_PLAYER_IMAGE ".\\img\\edwarblock bubblefen.bmp"
#define BUKAERA_IMAGE ".\\img\\gameOver_2.bmp"
#define MENUAREN_IRUDIA ".\\img\\portada.bmp"
#define KREDITUEN_IRUDIA ".\\img\\kredituak.bmp"
#define HASIERAKO_IRUDIA ".\\img\\hasierakoa.bmp"
#define PAUSA_MENUA ".\\img\\denda.bmp"
#define EXIT ".\\img\\exit.bmp"
#define KATANA_SHOT ".\\img\\katana2.bmp"
#define PISTOLA_SHOT ".\\img\\pistola.bmp"
#define AZKARRA_SHOT ".\\img\\azkarra.bmp"
#define RANKING_A ".\\img\\ranking.bmp"
#define SAL_MENU ".\\img\\salir_menua.bmp"
#define SAL_SCORE ".\\img\\score .bmp"

#define KATANA ".\\img\\katana.bmp"
#define TUTORIAL ".\\img\\tutorial.bmp"
#define DENDA ".\\img\\store.bmp"

#define BOSS1 ".\\img\\boss1.bmp"
#define BOSS2 ".\\img\\boss2.bmp"

#define BABEL ".\\img\\babel.bmp"
#define BALA_ARMA ".\\img\\bala.bmp"

		//---EL SONIDO---

#define MENUAREN_SOINUA ".\\sound\\menu.wav"

#define PUNTUAZIOAK ".\\sound\\ranking.wav"

#define EASY_MODE_MUSIC ".\\sound\\easy_mode.wav"
#define MEDIUM_MODE_MUSIC ".\\sound\\medium_mode.wav"
#define HARD_MODE_MUSIC ".\\sound\\hard_mode.wav"
#define APOCALIPTIC_MODE_MUSIC ".\\sound\\apoca_mode.wav"

#define INICIO_APOCALIPSIS ".\\sound\\zombie_apocalypse.wav"
#define MISSION_1_START ".\sound\mission 1 start.wav"
#define MISSION_2_START ".\sound\mission 2 start.wav"
#define MISSION_3_START ".\sound\mission 3 start.wav"
#define FINAL_MISSION_START ".\sound\mission final.wav"

#define JOKOA_SOUND_WIN ".\\sound\\kredituak.wav"
#define MUERTE ".\\sound\\dead.wav" 
#define BUKAERA_SOUND_1 ".\\sound\\dead.wav"
#define MONEY ".\\sound\\money.wav"
#define BUBBLE_KILL_SOUND ".\\sound\\bubble kill.wav"
#define SONIDO_KATANA ".\\sound\\sonidoKatana.wav"
#define KREDITAK_SOINUA ".\\sound\\kredituak.wav"
#define TUTORIALA ".\\sound\\tutorial.wav"
#define STORE_MUSIC ".\\sound\\shop.wav"







#endif // !HEADER_H
