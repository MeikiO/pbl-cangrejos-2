
#include "game02.h"

#ifndef MAILA_KUDEAKETA_H
#define MAILA_KUDEAKETA_H

ZAILTASUNA zailtasunen_kudeaketa(JOKALARIA jokalaria, MAILA* generatuta);

void pantaila_bakoitzaren_kudeaketa(ZAILTASUNA zailtasuna, MAILA* generatuta, int *id, JOKALARIA* jokalaria, TIMER katanaMomentua, JOKALARIA jokalaria_ametralladora[], BOSS boss, int arma_pantailaId, int arma);

#endif // !MAILA_KUDEAKETA_H
