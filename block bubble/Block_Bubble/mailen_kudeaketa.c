
#include "game02.h"

ZAILTASUNA zailtasunen_kudeaketa(JOKALARIA jokalaria, MAILA* generatuta)
{
	if (jokalaria.babel_kills < 10)//30
	{
		return EASY;
	}

	if ((jokalaria.babel_kills >= 10) && (jokalaria.babel_kills < 40))//100
	{
		return MEDIUM;
	}

	if ((jokalaria.babel_kills >= 40) && (jokalaria.babel_kills < 130)) //300
	{
		return HARD;
	}

	if (jokalaria.babel_kills >= 130)//1000
	{
		return APOCALYPTIC;
	}
}

void pantaila_bakoitzaren_kudeaketa(ZAILTASUNA zailtasuna, MAILA* generatuta, int *id, JOKALARIA* jokalaria, TIMER katanaMomentua, JOKALARIA jokalaria_ametralladora[], BOSS boss, int arma_pantailaId, int arma)
{
	int idAudioGame;
	switch (zailtasuna)
	{
	case EASY:
	{
		if (generatuta->easy == 0)  //bein bakarrik pasa behar duelako. joku modo bakoitzaren inizializazioak dira
		{
			generatuta->modo = 1;

			id = EASY_fondoa();

			if (katanaMomentua.tartea == 0)
			{
				jokalaria->id = JOKOA_jokalariaIrudiaSortu(katanaMomentua);
			}
			arma_pantailaId = Irudia_arma_azpikaldea(arma);
			pantailaBerriztu();
			audioInit();
			loadTheMusic(EASY_MODE_MUSIC);
			playMusic();
			generatuta->easy = 1;
		}
		break;
	}
	case MEDIUM:
	{
		if (generatuta->medium == 0)
		{
			generatuta->modo = 4;

			id = MEDIUM_fondoa();

			if (katanaMomentua.tartea == 0)
			{
				jokalaria->id = JOKOA_jokalariaIrudiaSortu(katanaMomentua);
			}
			jokalaria_hasieratu(&jokalaria, jokalaria_ametralladora);
			arma_pantailaId = Irudia_arma_azpikaldea(arma);
			pantailaBerriztu();

			audioTerminate();
			audioInit();

			loadTheMusic(MEDIUM_MODE_MUSIC);
			playMusic();
			generatuta->medium = 1;
		}
		break;
	}
	case HARD:
	{
		if (generatuta->hard == 0)
		{
			generatuta->modo = 9;

			id = HARD_fondoa();

			if (katanaMomentua.tartea == 0)
			{
				jokalaria->id = JOKOA_jokalariaIrudiaSortu(katanaMomentua);
			}

			jokalaria_hasieratu(&jokalaria, jokalaria_ametralladora);
			arma_pantailaId = Irudia_arma_azpikaldea(arma);
			pantailaBerriztu();

			audioTerminate();
			audioInit();

			loadTheMusic(HARD_MODE_MUSIC);
			playMusic();
			generatuta->hard = 1;
		}
		break;
	}
	case APOCALYPTIC:
	{
		if (generatuta->apocaliptic == 0)
		{
			generatuta->modo = 19;

			id = APOCALYPTIC_fondoa();

			if (katanaMomentua.tartea == 0)
			{
				jokalaria->id = JOKOA_jokalariaIrudiaSortu(katanaMomentua);
			}
			jokalaria_hasieratu(&jokalaria, jokalaria_ametralladora);
			arma_pantailaId = Irudia_arma_azpikaldea(arma);
			pantailaBerriztu();

			audioTerminate();
			audioInit();

			loadTheMusic(APOCALIPTIC_MODE_MUSIC);
			playMusic();
			generatuta->apocaliptic = 1;
		}
		break;
	}
	default:
		break;
	}
}