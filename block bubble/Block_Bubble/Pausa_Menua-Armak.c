#define _CRT_SECURE_NO_WARNINGS
#include "game02.h"


int pausaMenua(int arma, int* arma_pantailaId, EGOERA* egoera, JOKALARIA* jokalaria, BALA* bala)
{
	POSIZIOA pos;
	char str[125];
	int ebentu;
	int idPistola = 0;
	int idDenda = 0;
	int prezioa1 = 0;
	int prezioa2 = 500;
	int prezioa3 = 150;
	int prezioa4 = 150;
	int exit = 0;
	int times = 1;

	textuaGaitu();
	textuaIdatzi(100, 155, str);
	arkatzKoloreaEzarri(255, 255, 255);
	pantailaBerriztu();

	idDenda = JOKOA_pausaMenua();
	do
	{
		if (times == 1)
		{
			textuaGaitu();
			arkatzKoloreaEzarri(255, 255, 255);
			conversion_int_to_char(jokalaria->dirua, str);
			textuaIdatzi(330, 303, str);
			conversion_int_to_char(jokalaria->babel_kills, str);
			textuaIdatzi(347, 326, str);
			pantailaBerriztu();
			times = 0;
		}

		pos = saguarenPosizioa();
		ebentu = ebentuaJasoGertatuBada();
		if ((ebentu == SAGU_BOTOIA_EZKERRA) && (pos.x > 210) && (pos.x < 270) && (pos.y > 95) && (pos.y < 150) && (exit == 0)) //pistola
		{
			arma = 9;
			*arma_pantailaId = Irudia_arma_azpikaldea(arma);

			textuaGaitu();
			arkatzKoloreaEzarri(255, 255, 255);
			conversion_int_to_char(jokalaria->dirua, str);
			textuaIdatzi(330, 303, str);
			conversion_int_to_char(jokalaria->babel_kills, str);
			textuaIdatzi(347, 326, str);
			pantailaBerriztu();
		}
		if ((ebentu == SAGU_BOTOIA_EZKERRA) && (pos.x > 290) && (pos.x < 350) && (pos.y > 95) && (pos.y < 150) && (jokalaria->dirua >= prezioa2)) //katana
		{
			arma = 4;
			jokalaria->dirua -= 300;
			*arma_pantailaId = Irudia_arma_azpikaldea(arma);

			textuaGaitu();
			arkatzKoloreaEzarri(255, 255, 255);
			conversion_int_to_char(jokalaria->dirua, str);
			textuaIdatzi(330, 303, str);
			conversion_int_to_char(jokalaria->babel_kills, str);
			textuaIdatzi(347, 326, str);
			pantailaBerriztu();
		}
		if ((ebentu == SAGU_BOTOIA_EZKERRA) && (pos.x > 365) && (pos.x < 425) && (pos.y > 95) && (pos.y < 150) && (jokalaria->dirua >= prezioa3)) //p.azkarra
		{
			arma = 10;
			jokalaria->dirua -= 100;
			*arma_pantailaId = Irudia_arma_azpikaldea(arma);

			textuaGaitu();
			arkatzKoloreaEzarri(255, 255, 255);
			conversion_int_to_char(jokalaria->dirua, str);
			textuaIdatzi(330, 303, str);
			conversion_int_to_char(jokalaria->babel_kills, str);
			textuaIdatzi(347, 326, str);
			pantailaBerriztu();
		}
		//if ((ebentu == SAGU_BOTOIA_EZKERRA) && (pos.x > 210) && (pos.x < 270) && (pos.y > 170) && (pos.y < 225) && (jokalaria->dirua >= prezioa4)) //eskopeta //etorkizunerako gorde dugu
		//{
		//	arma = 8;
		//	jokalaria->dirua -= 15;

		//	textuaGaitu();
		//	arkatzKoloreaEzarri(255, 255, 255);
		//	conversion_int_to_char(jokalaria->dirua, str);
		//	textuaIdatzi(330, 303, str);
		//	conversion_int_to_char(jokalaria->babel_kills, str);
		//	textuaIdatzi(347, 326, str);
		//	pantailaBerriztu();
		//}
		//if ((ebentu == SAGU_BOTOIA_EZKERRA) && (pos.x > 150) && (pos.x < 200) && (pos.y > 500) && (pos.y < 600)) //Herio bonba //etorkizunerako gorde dugu
		//{
		//	arma = 7;
		//}
		if ((ebentu == SAGU_BOTOIA_EZKERRA) && (pos.x > 210) && (pos.x < 280) && (pos.y > 350) && (pos.y < 375)) //exit denda 
		{
			exit = 1;
		}
		if ((ebentu == SAGU_BOTOIA_EZKERRA) && (pos.x > 350) && (pos.x < 400) && (pos.y > 350) && (pos.y < 375)) //exit jokoa 
		{
			exit = 1;
			*egoera = GALDU;
		}
	} while (exit == 0);

	irudiaKendu(idDenda);

	return arma;
}

POSIZIOA bala_mugimendua(int abiadura, float mousepy, float mousepx, float angelua, POSIZIOA posizioa) //Arma mota: pistola
{
	int mousebatux = 15, mousebatuy = 15;

	if ((mousepy < 0) && (mousepx > 0))
	{//1 koadrantea
		mousebatux = abiadura * cos(angelua);
		mousebatuy = -abiadura * sin(angelua);
	}
	if ((mousepy > 0) && (mousepx < 0))
	{//2 koadrantea
		mousebatux = -abiadura * cos(angelua);
		mousebatuy = abiadura * sin(angelua);
	}
	if ((mousepy < 0) && (mousepx < 0))
	{//3 koadrantea
		mousebatux = -abiadura * cos(angelua);
		mousebatuy = -abiadura * sin(angelua);
	}
	if ((mousepy > 0) && (mousepx > 0))
	{//4 koadrantea
		mousebatux = abiadura * cos(angelua);
		mousebatuy = abiadura * sin(angelua);
	}

	posizioa.x = posizioa.x + mousebatux;
	posizioa.y = posizioa.y + mousebatuy; // mugimendu abiadura = 8u/t

	return posizioa;
}
void armak_bala_mugimendua(JOKALARIA *jokalaria, BALA bala[], SAGUA gure_sagua, int arma, TIMER* katanaMomentua, JOKALARIA jokalaria_ametralladora[], SAGUA gure_sagua_multiple[])
{
	int idAudioGame;

	switch (arma)
	{
	case 9://Arma mota: Pistolak 

		if ((jokalaria->tiroa) && (bala[0].pos.x > -10) && (bala[0].pos.x < 650) && (bala[0].pos.y > -10) && (bala[0].pos.y < 480))
		{
			bala[0].pos = bala_mugimendua(arma, gure_sagua.mousepy, gure_sagua.mousepx, jokalaria->tiro_angelua, bala[0].pos);
			irudiaMugitu(bala[0].id, bala->pos.x, bala->pos.y);
		}
		else
		{
			jokalaria->tiroa = 0;
		}
		break;

	case 10: //ametralladora

		if (jokalaria_ametralladora[0].tiroa)
		{
			if ((bala[0].pos.x > -10) && (bala[0].pos.x < 650) && (bala[0].pos.y > -10) && (bala[0].pos.y < 480))
			{
				bala[0].pos = bala_mugimendua(arma, gure_sagua_multiple[0].mousepy, gure_sagua_multiple[0].mousepx, jokalaria_ametralladora[0].tiro_angelua, bala[0].pos);
				irudiaMugitu(bala[0].id, bala[0].pos.x, bala[0].pos.y);
			}
			else
			{
				irudiaKendu(bala[0].id);
				jokalaria_ametralladora[0].tiroa = 0;
			}
		}
	
		if (jokalaria_ametralladora[1].tiroa)
		{
			if ((bala[1].pos.x > -10) && (bala[1].pos.x < 650) && (bala[1].pos.y > -10) && (bala[1].pos.y < 480))
			{
				bala[1].pos = bala_mugimendua(arma, gure_sagua_multiple[1].mousepy, gure_sagua_multiple[1].mousepx, jokalaria_ametralladora[1].tiro_angelua, bala[1].pos);
				irudiaMugitu(bala[1].id, bala[1].pos.x, bala[1].pos.y);
			}

			else
			{
				irudiaKendu(bala[1].id);
				jokalaria_ametralladora[1].tiroa = 0;
			}
		}

		if (jokalaria_ametralladora[2].tiroa)
		{
			if ((bala[2].pos.x > -10) && (bala[2].pos.x < 650) && (bala[2].pos.y > -10) && (bala[0].pos.y < 480))
			{
				bala[2].pos = bala_mugimendua(arma, gure_sagua_multiple[2].mousepy, gure_sagua_multiple[2].mousepx, jokalaria_ametralladora[2].tiro_angelua, bala[2].pos);
				irudiaMugitu(bala[2].id, bala[2].pos.x, bala[2].pos.y);
			}

			else
			{
				irudiaKendu(bala[2].id);
				jokalaria_ametralladora[2].tiroa = 0;
			}
		}

		if (jokalaria_ametralladora[3].tiroa)
		{
			if ((bala[3].pos.x > -10) && (bala[3].pos.x < 650) && (bala[3].pos.y > -10) && (bala[3].pos.y < 480))
			{
				bala[3].pos = bala_mugimendua(arma, gure_sagua_multiple[3].mousepy, gure_sagua_multiple[3].mousepx, jokalaria_ametralladora[3].tiro_angelua, bala[3].pos);
				irudiaMugitu(bala[3].id, bala[3].pos.x, bala[3].pos.y);
			}
			else
			{
				irudiaKendu(bala[3].id);
				jokalaria_ametralladora[3].tiroa = 0;
			}
		}

		if (jokalaria_ametralladora[4].tiroa)
		{
			if ((bala[4].pos.x > -10) && (bala[4].pos.x < 650) && (bala[4].pos.y > -10) && (bala[4].pos.y < 480))
			{
				bala[4].pos = bala_mugimendua(arma, gure_sagua_multiple[4].mousepy, gure_sagua_multiple[4].mousepx, jokalaria_ametralladora[4].tiro_angelua, bala[4].pos);
				irudiaMugitu(bala[4].id, bala[4].pos.x, bala[4].pos.y);
			}
			else
			{
				irudiaKendu(bala[4].id);
				jokalaria_ametralladora[4].tiroa = 0;
			}
		}
		
		if (jokalaria_ametralladora[5].tiroa)
		{
			if ((bala[5].pos.x > -10) && (bala[5].pos.x < 650) && (bala[5].pos.y > -10) && (bala[5].pos.y < 480))
			{
				bala[5].pos = bala_mugimendua(arma, gure_sagua_multiple[5].mousepy, gure_sagua_multiple[5].mousepx, jokalaria_ametralladora[5].tiro_angelua, bala[5].pos);
				irudiaMugitu(bala[5].id, bala[5].pos.x, bala[5].pos.y);
			}
			else
			{
				irudiaKendu(bala[5].id);
				jokalaria_ametralladora[5].tiroa = 0;
			}
		}

		if (jokalaria_ametralladora[6].tiroa)
		{
			if ((bala[6].pos.x > -10) && (bala[6].pos.x < 650) && (bala[6].pos.y > -10) && (bala[6].pos.y < 480))
			{
				bala[6].pos = bala_mugimendua(arma, gure_sagua_multiple[6].mousepy, gure_sagua_multiple[6].mousepx, jokalaria_ametralladora[6].tiro_angelua, bala[6].pos);
				irudiaMugitu(bala[6].id, bala[6].pos.x, bala[6].pos.y);
			}
			else
			{
				irudiaKendu(bala[6].id);
				jokalaria_ametralladora[6].tiroa = 0;
				jokalaria->kont = 0; //CONTADOR UNIVERSAL
			}
		}

	case 4://KATANA
		if ((jokalaria->tiroa == 1) && (katanaMomentua->tartea == 0) && (katanaMomentua->denboraHasiera > 90))
		{
			irudiaKendu(jokalaria->id);

			katanaMomentua->pos.x = jokalaria->pos.x + 20;
			katanaMomentua->pos.y = jokalaria->pos.y + 20;

			katanaMomentua->tartea = 1;
			katanaMomentua->denboraBukaera = katanaMomentua->denboraHasiera;
			*katanaMomentua = JOKOA_katana(*katanaMomentua);
			jokalaria->tiroa = 0;

			idAudioGame = loadSound(SONIDO_KATANA);
			playSound(idAudioGame);
		}
		else {
			jokalaria->tiroa = 0;
		}
		if ((katanaMomentua->denboraHasiera - katanaMomentua->denboraBukaera > 20) && (katanaMomentua->tartea == 1))
		{
			irudiaKendu(katanaMomentua->id);
			jokalaria->id = JOKOA_jokalariaIrudiaSortu(*katanaMomentua);
			jokalaria->pos.x = katanaMomentua->pos.x - 20;
			jokalaria->pos.y = katanaMomentua->pos.y - 20;

			katanaMomentua->denboraHasiera = 0;
			katanaMomentua->tartea = 0;
		}
		irudiaMugitu(katanaMomentua->id, katanaMomentua->pos.x - 50, katanaMomentua->pos.y - 52);
		irudiakMarraztu();
		pantailaBerriztu();
		break;
	}
}

int JOKOA_pausaMenua()
{
	int pmenuId = -1;
	pmenuId = irudiaKargatu(PAUSA_MENUA);
	irudiaMugitu(pmenuId, -20, 15);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return pmenuId;
}

int Irudia_arma_azpikaldea(int arma)
{
	int arma_azpiId = -1;

	if (arma==9)
	{
		arma_azpiId = irudiaKargatu(PISTOLA_SHOT);
	}
	if (arma == 10)
	{
		arma_azpiId = irudiaKargatu(AZKARRA_SHOT);
	}
	if (arma == 4)
	{
		arma_azpiId = irudiaKargatu(KATANA_SHOT);
	}
	
	irudiaMugitu(arma_azpiId, 5, 400);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return arma_azpiId;
}

TIMER JOKOA_katana(TIMER katanaMomentua)
{
	katanaMomentua.id = irudiaKargatu(KATANA);
	irudiaMugitu(katanaMomentua.id, katanaMomentua.pos.x - 50, katanaMomentua.pos.y - 52);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return katanaMomentua;
}

int Irudia_bala(BALA bala, JOKALARIA jokalaria) //mete id a bala
{
	int posizioa;

	bala.id = -1;
	bala.id = irudiaKargatu(BALA_ARMA);
	irudiaMugitu(bala.id, jokalaria.pos.x + 10, jokalaria.pos.y + 10);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return bala.id;
}