
#include "game02.h"

int JOKOA_jokalariaIrudiaSortu(TIMER katanaMomentua)
{
		int martzianoId = -1;
		martzianoId = irudiaKargatu(JOKOA_PLAYER_IMAGE);
		irudiaMugitu(martzianoId, katanaMomentua.pos.x - 20, katanaMomentua.pos.y - 20);
		pantailaGarbitu();
		irudiakMarraztu();
		pantailaBerriztu();
		return martzianoId;
}

JOKALARIA jokalariaren_mugimenduak(int ebentu, JOKALARIA jokalaria, SAGUA gure_sagua, TIMER* katanaMomentua, JOKALARIA* jokalaria_ametralladora, SAGUA* gure_sagua_multiple, int arma, BALA bala[])
{
	int kont = 0;

	const Uint8 * state = SDL_GetKeyboardState(NULL);

	if ((state[SDL_SCANCODE_D]) && (jokalaria.pos.x < 613))
	{
		if (katanaMomentua->tartea == 0)
		{
			jokalaria.pos.x = jokalaria.pos.x + 2;
		}
		else
		{
			katanaMomentua->pos.x = katanaMomentua->pos.x + 2;
		}
	}
	if ((state[SDL_SCANCODE_A]) && (jokalaria.pos.x > 0))
	{
		if (katanaMomentua->tartea == 0)
		{
			jokalaria.pos.x = jokalaria.pos.x - 2;
		}
		else
		{
			katanaMomentua->pos.x = katanaMomentua->pos.x - 2;
		}
	}
	if ((state[SDL_SCANCODE_W]) && (jokalaria.pos.y > 0))
	{
		if (katanaMomentua->tartea == 0)
		{
			jokalaria.pos.y = jokalaria.pos.y - 2;
		}
		else
		{
			katanaMomentua->pos.y = katanaMomentua->pos.y - 2;
		}
	}
	if ((state[SDL_SCANCODE_S]) && (jokalaria.pos.y < 455))
	{
		if (katanaMomentua->tartea == 0)
		{
			jokalaria.pos.y = jokalaria.pos.y + 2;
		}
		else
		{
			katanaMomentua->pos.y = katanaMomentua->pos.y + 2;
		}
	}
	if (ebentu == TECLA_m)
	{
		toggleMusic();
	}

	if ((ebentu == SAGU_BOTOIA_EZKERRA) && (jokalaria.tiroa == 0) && (arma==4)) //katana. arrautzak ez uzteko
	{
		jokalaria.tiroa = 1;
		jokalaria.tiro_angelua = atan2(abs(gure_sagua.mousepy), abs(gure_sagua.mousepx));
	}

	if ((ebentu == SAGU_BOTOIA_EZKERRA) && (jokalaria.tiroa == 0) && (arma==9)) //pistola
	{
		bala[0].id = Irudia_bala(bala[0], jokalaria);
		bala[0].pos.x = jokalaria.pos.x;
		bala[0].pos.y = jokalaria.pos.y;

		jokalaria.tiroa = 1;
		jokalaria.tiro_angelua = atan2(abs(gure_sagua.mousepy), abs(gure_sagua.mousepx));
	}

	if ((ebentu == SAGU_BOTOIA_EZKERRA) && (jokalaria.tiroa == 0) && (arma == 8)) //pistola
	{
		bala[0].id = Irudia_bala(bala[0], jokalaria);
		bala[0].pos.x = jokalaria.pos.x;
		bala[0].pos.y = jokalaria.pos.y;

		bala[1].id = Irudia_bala(bala[1], jokalaria);
		bala[1].pos.x = jokalaria.pos.x;
		bala[1].pos.y = jokalaria.pos.y;

		bala[2].id = Irudia_bala(bala[0], jokalaria);
		bala[2].pos.x = jokalaria.pos.x;
		bala[2].pos.y = jokalaria.pos.y;

		bala[3].id = Irudia_bala(bala[3], jokalaria);
		bala[3].pos.x = jokalaria.pos.x;
		bala[3].pos.y = jokalaria.pos.y;

		jokalaria.tiroa = 1;
		jokalaria.tiro_angelua = atan2(abs(gure_sagua.mousepy), abs(gure_sagua.mousepx));
	}

	if ((ebentu == SAGU_BOTOIA_EZKERRA) && (jokalaria_ametralladora[6].tiroa!=1) && (arma == 10)) //pistola azkarra
	{
		bala[jokalaria.kont].id = Irudia_bala(bala[jokalaria.kont], jokalaria);
		bala[jokalaria.kont].pos.x = jokalaria.pos.x;
		bala[jokalaria.kont].pos.y = jokalaria.pos.y;

		sagua_pos_multiple(gure_sagua, gure_sagua_multiple, jokalaria);

		jokalaria_ametralladora[jokalaria.kont].tiroa = 1;
		jokalaria_ametralladora[jokalaria.kont].tiro_angelua = atan2(abs(gure_sagua_multiple[jokalaria.kont].mousepy), abs(gure_sagua_multiple[jokalaria.kont].mousepx));

		jokalaria.kont++;
	}
	return jokalaria;
}

SAGUA sagua_pos(SAGUA gure_arratoia,JOKALARIA jokalaria)
{

	gure_arratoia.mousepy = gure_arratoia.mousey - jokalaria.pos.y;
	gure_arratoia.mousepx = gure_arratoia.mousex - jokalaria.pos.x;

	return gure_arratoia;

}
void sagua_pos_multiple(SAGUA gure_sagua, SAGUA* gure_sagua_multiple, JOKALARIA jokalaria)
{

	gure_sagua_multiple[jokalaria.kont].mousepy = gure_sagua.mousey - jokalaria.pos.y;
	gure_sagua_multiple[jokalaria.kont].mousepx = gure_sagua.mousex - jokalaria.pos.x;
}

void jokalaria_hasieratu(JOKALARIA *jokalaria, JOKALARIA jokalaria_ametralladora[])
{
	int i=0;

	jokalaria->pos.x = 320;
	jokalaria->pos.y = 220;

	jokalaria->tiroa = 0;
	jokalaria->kont = 0;

	for (i ; i <= 6; i++)
	{
		jokalaria_ametralladora[i].tiroa = 0;
	}
}