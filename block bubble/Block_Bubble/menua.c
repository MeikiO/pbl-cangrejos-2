
#define _CRT_SECURE_NO_WARNINGS
#include "game02.h"


int main(void)
{
	if (sgHasieratu() == -1)
	{
		fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());
		return 1;
	}

	audioInit();
	loadTheMusic(MONEY);
	playMusic();


	int fondoa = irudiaKargatu(HASIERAKO_IRUDIA);

	while (ebentuaJasoGertatuBada() != TECLA_RETURN)
	{
		pantailaGarbitu();
		irudiakMarraztu();
		pantailaBerriztu();
	}

	irudiaKendu(HASIERAKO_IRUDIA);


	menua();

}



int menua()
{


	EGOERA egoera;
	POSIZIOA pos;
	int ebentu = 0, irten = 0;




	while (!irten)
	{
		pantailaGarbitu();
		hasieratu_menua();

		while (1)
		{

			ebentu = ebentuaJasoGertatuBada();

			if (ebentu == TECLA_m)
			{
				toggleMusic();
			}

			if (ebentu == SAGU_BOTOIA_EZKERRA)
			{

				pos = saguarenPosizioa();

				if ((pos.x > 290) && (pos.x < 405) && (pos.y > 130) && (pos.y < 150))//PLAY botoiari dagokio
				{
					menua_itxi();
					irudiaKendu(MENUAREN_IRUDIA);
					egoera = jokatu();

				}
				else if ((pos.x > 294) && (pos.x < 379) && (pos.y > 232) && (pos.y < 250))// RANKING dagokio
				{
					menua_itxi();
					vistaratu_ranking();
					//Ranking-a egin txt a ikusteko soilik.
				}
				if ((pos.x > 294) && (pos.x < 379) && (pos.y > 179) && (pos.y < 197))//TUTORIAL botoiari dagokio
				{
					menua_itxi();
					tutoriala();
				}
				else if ((pos.x > 285) && (pos.x < 350) && (pos.y > 402) && (pos.y < 425))//EXIT botoiari dagokio
				{

					irudiaKendu(MENUAREN_IRUDIA);
					break;
				}
				else if ((pos.x > 294) && (pos.x < 379) && (pos.y > 282) && (pos.y < 305))//CREDITS botoiari dagokio
				{
					menua_itxi();
					credits();
				}
				if ((pos.x > 277) && (pos.x < 360) && (pos.y > 323) && (pos.y < 357))///Denda botoiari dagokio
				{
					menua_itxi();
					denda();
				}

			}
			else if (ebentu == TECLA_ESCAPE)
			{
				irten = 1;
			}

			pantailaBerriztu();

		}
		audioTerminate();
		sgItxi();
		return 0;
	}
}


void hasieratu_menua(void)
{
	int fondoa = irudiaKargatu(MENUAREN_IRUDIA);;

	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();

	audioInit();
	loadTheMusic(MENUAREN_SOINUA);
	playMusic();

}

void menua_itxi()
{
	irudiaKendu(MENUAREN_IRUDIA);
	audioTerminate();
}

void tutoriala(void)
{
	int tutorialaren_fondoa = irudiaKargatu(TUTORIAL);

	audioInit();
	loadTheMusic(TUTORIALA);
	playMusic();

	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();

	while (1)
	{
		if (ebentuaJasoGertatuBada() == TECLA_ESCAPE)
		{
			irudiaKendu(TUTORIALA);
			break;
		}

	}
	hasieratu_menua();
}

void denda(void)
{
	int tutorialaren2_fondoa = irudiaKargatu(DENDA);


	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();

	audioInit();
	loadTheMusic(STORE_MUSIC);
	playMusic();

	while (1)
	{
		if (ebentuaJasoGertatuBada() == TECLA_ESCAPE)
		{
			irudiaKendu(DENDA);
			break;
		}
	}

	hasieratu_menua();

}

void credits()
{
	int Kredituen_Fondoa = irudiaKargatu(KREDITUEN_IRUDIA);

	audioInit();
	loadTheMusic(KREDITAK_SOINUA);
	playMusic();

	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();


	while (1)
	{
		if (ebentuaJasoGertatuBada() == TECLA_ESCAPE)
		{
			irudiaKendu(KREDITUEN_IRUDIA);
			break;
		}

	}

	hasieratu_menua();

}


void vistaratu_ranking()
{
	RANKING zerrenda[10];

	int i = 0;

	char zenbaki_textifikatuak[100];

	POSIZIOA rank;

	rank.x = 75, rank.y = 40;

	pantailaGarbitu();

	textuaGaitu();

	audioInit();

	loadTheMusic(PUNTUAZIOAK);

	playMusic();

	irudiaKargatu(RANKING_A);

	ranking_zerrenda_kargatu(zerrenda);


	//emen idatziak sartzen dira

	//textuak idatzi funtzioak ezin dituenez int aldagaiak idatzi char etara pasatzen ditugu.

	//nota zenbakiak ez dira ondo idazten.

	pantailaGarbitu();

	irudiakMarraztu();

	do
	{
		conversion_int_to_char(zerrenda[i].posizioa, zenbaki_textifikatuak);

		textuaIdatzi(rank.x + 100, rank.y, zenbaki_textifikatuak);

		textuaIdatzi(rank.x + 200, rank.y, zerrenda[i].jokalariaren_izena);


		conversion_int_to_char(zerrenda[i].zonbie_kills, zenbaki_textifikatuak);

		textuaIdatzi(rank.x + 300, rank.y, zenbaki_textifikatuak);

		rank.y = rank.y + 40;

		i++;

		arkatzKoloreaEzarri(255, 255, 255);
		pantailaBerriztu();

	} while (i <= 9);

	while (1)
	{
		if (ebentuaJasoGertatuBada() == TECLA_ESCAPE)
		{

			break;
		}

	}

	audioTerminate();
	irudiaKendu(RANKING_A);
	textuaDesgaitu();

	hasieratu_menua();

}


char conversion_int_to_char(int zenbakia, char buffer[])
{
	sprintf(buffer, "%3d", zenbakia);

	//sprintf(cadena a la que se quiere meter el numero, formato, numero);

}