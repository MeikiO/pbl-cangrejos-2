
#include "game02.h"

#ifndef ENEMIGOS_H
#define ENEMIGOS_H

JOKALARIA etsaiaren_mugimenduak(JOKALARIA jokalaria, ETSAIA babel[], EGOERA* egoera, BALA bala[], TIMER katanaMomentua, int arma, JOKALARIA jokalaria_ametralladora[], MAILA generatuta, BOSS boss);

ETSAIA akabatu_babel(BALA bala[], ETSAIA oztopoa, JOKALARIA *jokalaria, JOKALARIA jokalaria_ametralladora[], TIMER katanaMomentua, int arma, MAILA* generatuta, BOSS boss);

ETSAIA Irudia_babel(ETSAIA babel);

void etsaia_sortu(ZAILTASUNA zailtasuna, ETSAIA bubble[], JOKALARIA* jokalaria, MAILA* generatuta);



BOSS boss_mugimendua(BOSS boss, ZAILTASUNA zailtasuna, JOKALARIA jokalaria, EGOERA* egoera);

BOSS Irudia_boss(BOSS boss, JOKALARIA jokalaria, ZAILTASUNA zailtasuna);

BOSS akabatu_boss(BALA bala[], BOSS boss, JOKALARIA *jokalaria, JOKALARIA jokalaria_ametralladora[], TIMER katanaMomentua, int arma, MAILA generatuta, ZAILTASUNA zailtasuna, BALA bala_boss[]);

void boss_tiroa(BOSS boss, BALA bala_boss[], EGOERA* egoera, JOKALARIA jokalaria);

void boss_bala_generazioa(BOSS boss, BALA bala_boss[]);

int Irudia_bala_boss(BOSS boss);


#endif // !ENEMIGOS_H
