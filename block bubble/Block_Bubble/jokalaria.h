
#include "OurTypes.h"

#ifndef JOKALARIA_H
#define JOKALARIA_H


//JOKALARIAK ERABILTZEN DITUENAK

int JOKOA_jokalariaIrudiaSortu(TIMER katanaMomentua);

JOKALARIA jokalariaren_mugimenduak(int ebentu, JOKALARIA jokalaria, SAGUA gure_sagua, TIMER* katanaMomentua, JOKALARIA* jokalaria_ametralladora, SAGUA* gure_sagua_multiple, int arma, BALA bala[]);
SAGUA sagua_pos(SAGUA gure_arratoia, JOKALARIA jokalaria);
void sagua_pos_multiple(SAGUA gure_sagua, SAGUA* gure_sagua_multiple, JOKALARIA jokalaria);
void jokalaria_hasieratu(JOKALARIA *jokalaria, JOKALARIA jokalaria_ametralladora[]);

#endif // !JOKALARIA_H
