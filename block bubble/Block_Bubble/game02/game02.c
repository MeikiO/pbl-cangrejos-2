
#define _CRT_SECURE_NO_WARNINGS
#include "game02.h"

EGOERA jokatu(void) 
{

  irudiaKargatu(JOKOA_PLAYER_IMAGE);
 
  int kont=0;
  int id;
  int mugitu = 0; //boolean
  int ebentu = 0;
  int arma=9;
  int jokoAbiadura = 5;
  int arma_pantailaId;
  int kont_armak = 0;

  ZAILTASUNA zailtasuna=EASY;		
  ETSAIA babel[260]; 
  BOSS boss;
  MAILA generatuta;

  BALA bala[16], bala_boss[4];
  TIMER katanaMomentua;
  SAGUA gure_sagua, gure_sagua_multiple[15];

  JOKALARIA jokalaria, jokalaria_ametralladora[15];
  EGOERA  egoera = JOLASTEN;

  srand(time(0));

  jokalaria.babel_kills = 0;
  jokalaria.dirua = 0;

  /////////////////////////////////////katana
  katanaMomentua.tartea = 0;
  katanaMomentua.denboraHasiera = 0;
  katanaMomentua.denboraBukaera = 40;
  katanaMomentua.id = 100;
  katanaMomentua.pos.x = 300;
  katanaMomentua.pos.x = 500;
  ///////////////////////////////////babels
  babel->bizirik_al_dago = 0;
  generatuta.easy = 0;
  generatuta.medium = 0;
  generatuta.hard = 0;
  generatuta.apocaliptic = 0;
  generatuta.kont = 0;
  generatuta.modo = 2;
  jokalaria.pantailan_kopurua = 0;
  ///////////////////////////////////BOSS
  boss.movx = 1;
  boss.movy = 1;
  boss.sortuta = 0;
  boss.beharrezko_killak = 30; //zenbat jokalaria.babel_kills-ero agertuko da bossa? 
  boss.jasotako_tiroak = 0;
  boss.hilda = 0;
  //////////////////////////////////

  jokalaria_hasieratu(&jokalaria, jokalaria_ametralladora);

  arma_pantailaId = Irudia_arma_azpikaldea(arma);

  do {
	  Sleep(jokoAbiadura);

	  zailtasuna = zailtasunen_kudeaketa(jokalaria, &generatuta);

	  pantaila_bakoitzaren_kudeaketa(zailtasuna, &generatuta, &id, &jokalaria, katanaMomentua, jokalaria_ametralladora, boss, arma_pantailaId, arma);
	  irudiaMugitu(jokalaria.id, jokalaria.pos.x, jokalaria.pos.y);

	  etsaia_sortu(zailtasuna, babel, &jokalaria, &generatuta);  //etsaia kargatu eta posizioa eman (x1 aldiz)

	  jokalaria = etsaiaren_mugimenduak(jokalaria, babel, &egoera, bala, katanaMomentua, arma, jokalaria_ametralladora, generatuta, boss); //etsaia mugitu eta kendu akabatzen bada


	  boss.hilda = 0; //bossa hiltzean pantailako babel guztiak akabatzeko.

	  if ((boss.sortuta == 0) && (jokalaria.babel_kills == boss.beharrezko_killak)) //bossa jokalaria.babel_kills-n arabera agertuko da
	  {
		  boss_bala_generazioa(boss, bala_boss);
		  boss = Irudia_boss(boss, jokalaria, zailtasuna);
		  boss.sortuta = 1;
	  }

	  boss = akabatu_boss(bala, boss, &jokalaria, jokalaria_ametralladora, katanaMomentua, arma, generatuta, zailtasuna, bala_boss);
	  boss = boss_mugimendua(boss, zailtasuna, jokalaria, &egoera);
	  boss_tiroa(boss, bala_boss, &egoera, jokalaria);
	  irudiaMugitu(boss.id, boss.pos.x, boss.pos.y);

	  if (jokalaria.pantailan_kopurua==0)
	  {
		  generatuta.kont = 0;
	  }


	  ebentu = ebentuaJasoGertatuBada();
	  SDL_GetMouseState(&gure_sagua.mousex, &gure_sagua.mousey);

		if ((ebentu == TECLA_q) && (katanaMomentua.tartea == 0) && (jokalaria_ametralladora[0].tiroa == 0) && (jokalaria_ametralladora[1].tiroa == 0) && (jokalaria_ametralladora[2].tiroa == 0) && (jokalaria_ametralladora[3].tiroa == 0) && (jokalaria_ametralladora[4].tiroa == 0) && (jokalaria_ametralladora[5].tiroa == 0) && (jokalaria_ametralladora[6].tiroa == 0) && jokalaria.tiroa == 0) //PISTOLARENTZAT
		{
			arma = pausaMenua(arma, &arma_pantailaId, &egoera, &jokalaria, &bala);
		}

		if ((jokalaria.tiroa == 0))
		{
			gure_sagua = sagua_pos(gure_sagua, jokalaria);
		}

	  jokalaria = jokalariaren_mugimenduak(ebentu, jokalaria, gure_sagua, &katanaMomentua, jokalaria_ametralladora, gure_sagua_multiple, arma, bala);
	  armak_bala_mugimendua(&jokalaria, bala, gure_sagua, arma, &katanaMomentua, jokalaria_ametralladora, gure_sagua_multiple);

	  katanaMomentua.denboraHasiera++;

	  irudiakMarraztu();  
	  pantailaBerriztu(); 
	  pantailaGarbitu();

  } while (egoera == JOLASTEN);

  irudiaKendu(jokalaria.id);
  irudiaKendu(JOKOA_PLAYER_IMAGE);
  toggleMusic();
  audioTerminate();
  pantailaGarbitu();
  pantailaBerriztu();

  Jokoa_itxi(egoera, jokalaria.babel_kills);

  return egoera;
}

int  Jokoa_itxi(EGOERA egoera, int babel_kils)
{
	textuaGaitu();

	POSIZIOA pos;

	int ebentu = 0, id, id2, id3, id4;
	int idAudioGame;
	int prob = 0;
	char buffer[125];

	pantailaGarbitu();
	pantailaBerriztu();

	if (egoera == GALDU)
	{
		idAudioGame = loadSound(MUERTE);
		playSound(idAudioGame);
		id = BUKAERA_irudiaBistaratu();
		id2 = BUKAERA_exit();
		id3 = BUK_salir_menua();
		id4 = BUK_salir_score();

		conversion_int_to_char(babel_kils, buffer);
		textuaIdatzi(520, 412, buffer);
		arkatzKoloreaEzarri(255, 255, 255);
		pantailaBerriztu();

	}

	//ranking
	puntuazioa_rankinean_sartu(babel_kils);

	do
	{
		ebentu = ebentuaJasoGertatuBada();

		if (ebentu == SAGU_BOTOIA_EZKERRA)
		{
			pos = saguarenPosizioa();

			if ((pos.x > 130) && (pos.x < 222) && (pos.y > 402) && (pos.y < 425))//MENURA bueltatzeko
			{
				prob = 1;
			}
			if ((pos.x > 285) && (pos.x < 390) && (pos.y > 402) && (pos.y < 435))
			{
				prob = 2;
			}
		}

	} while (prob == 0);

	irudiaKendu(id);
	irudiaKendu(id2);
	irudiaKendu(id3);
	irudiaKendu(id4);
	textuaDesgaitu();

	irudiaKendu(JOKOA_PLAYER_IMAGE);

	if (prob == 1)
	{
		//irudiaKendu(MENUAREN_IRUDIA);
		//irudiaKendu(JOKOA_PLAYER_IMAGE);
		menua();
	}
	return (ebentu != TECLA_RETURN) ? 1 : 0;
}

int BUKAERA_irudiaBistaratu()
{
	int id = -1;
	id = irudiaKargatu(BUKAERA_IMAGE);
	irudiaMugitu(id, 87, 170);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return id;
}

int BUKAERA_exit()
{
	int id = -1;
	id = irudiaKargatu(EXIT);
	irudiaMugitu(id, 285, 402);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return id;
}

int BUK_salir_menua()
{
	int id = -1;
	id = irudiaKargatu(SAL_MENU);
	irudiaMugitu(id, 150, 407);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return id;
}

int BUK_salir_score()
{
	int id = -1;
	id = irudiaKargatu(SAL_SCORE);
	irudiaMugitu(id, 400, 407);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return id;
}

void puntuazioa_rankinean_sartu(int babel_kils)
{

	RANKING gure_jokalaria;

	RANKING zerrenda[10];

	RANKING tmp;

	FILE *fp;
	int chara;
	int i = 0;

	/*
	textuaGaitu();

	textuaIdatzi(200, 25, "Sartu jokalariaren izena:");

	textuaDesgaitu();
	*/

	strcpy(gure_jokalaria.jokalariaren_izena, "JOKALARIA");

	gure_jokalaria.zonbie_kills = babel_kils;

	ranking_zerrenda_kargatu(zerrenda);

	do
	{
		if (gure_jokalaria.zonbie_kills > zerrenda[i].zonbie_kills)
		{
			gure_jokalaria.posizioa = zerrenda[i].posizioa;

			tmp = zerrenda[i];
			zerrenda[i] = gure_jokalaria;
			puntuaMarraztu(500, 200);
			gure_jokalaria = tmp;
		}
		else
		{
			i++;
		}

	} while (i <= 9);

	//orain ordenatutako arraia fitxerora sartzen dugu.

	i = 0;

	fp = fopen("ranking.txt", "w");

	do
	{
		chara = fprintf(fp, "%d %s %d\n", zerrenda[i].posizioa
			, zerrenda[i].jokalariaren_izena,
			zerrenda[i].zonbie_kills);

		i++;
	} while (i <= 9);

	fclose(fp);
}

RANKING* ranking_zerrenda_kargatu(RANKING ranking_jokalariak[])
{
	unsigned int i = 0;

	FILE *fp;
	int chara = 0;

	//fitxategia irekitzen dugu aldaketak gauzatzeko

	fp = fopen("ranking.txt", "r");

	if (fp == NULL)
	{
		printf("\n \n ez da kargatu");
	}
	else
	{
		//fitxategiko elementu guztira struktura batetara sartzen ditugu konparatzeko

		while ((chara != -1) && (i < 10))
		{

			chara = fscanf(fp, "%d %s %d \n", &ranking_jokalariak[i].posizioa, ranking_jokalariak[i].jokalariaren_izena, &ranking_jokalariak[i].zonbie_kills);

			i++;

		}
		fclose(fp);
	}
	return ranking_jokalariak;
}


int EASY_fondoa()
{
	int id = -1;
	id = irudiaKargatu(EASY_FONDO);
	irudiaMugitu(id, 0, 0);
	return id;
}

int MEDIUM_fondoa()
{
	int id = -1;
	id = irudiaKargatu(MEDIUM_FONDO);
	irudiaMugitu(id, 0, 0);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return id;
}

int HARD_fondoa()
{
	int id = -1;
	id = irudiaKargatu(HARD_FONDO);
	irudiaMugitu(id,0, 0);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return id;
}

int APOCALYPTIC_fondoa()
{
	int id = -1;
	id = irudiaKargatu(APOCALYPTIC_FONDO);
	irudiaMugitu(id, 0, 0);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return id;
}