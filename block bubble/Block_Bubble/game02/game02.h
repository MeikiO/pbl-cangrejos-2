#ifndef GAME10_H
#define GAME10_H

#include "ourTypes.h"

#include "includes.h"

//TIMER
Uint32 SDL_GetTicks(void);

//MENURAKO ERABILTZEN DIRENAK
void hasieratu_menua(void);
void menua_itxi();
void credits();
void vistaratu_ranking();
char conversion_int_to_char(int zenbakia, char buffer[]);
void tutoriala(void);
void denda(void);


//JOKOAN ZEHARREKOAK
EGOERA jokatu(void);
int  Jokoa_itxi(EGOERA egoera, int zonbie_kils);


//Jokoaren background
int EASY_fondoa();
int MEDIUM_fondoa();
int HARD_fondoa();
int APOCALYPTIC_fondoa();

//Armakin zerikusia dutenak
int Irudia_bala(BALA bala, JOKALARIA jokalaria);
int Irudia_arma_azpikaldea(int arma);
TIMER JOKOA_katana(TIMER katanaMomentua);
int pausaMenua(int arma, int* arma_pantailaId, EGOERA* egoera, JOKALARIA* jokalaria, BALA* bala);
POSIZIOA bala_mugimendua(int abiadura, float mousepy, float mousepx, float angelua, POSIZIOA posizioa);
void armak_bala_mugimendua(JOKALARIA *jokalaria, BALA bala[], SAGUA gure_sagua, int arma, TIMER* katanaMomentua, JOKALARIA jokalaria_ametralladora[], SAGUA gure_sagua_multiple[]);
///////////////////////////////////

int BUKAERA_irudiaBistaratu();

void puntuazioa_rankinean_sartu(int zonbie_kils);
RANKING* ranking_zerrenda_kargatu(RANKING ranking_jokalariak[]);

/////Joko itxi Game Over                                                       
int BUKAERA_exit();

#endif