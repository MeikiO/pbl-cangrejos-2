#include "enemigos.h"
#include "game02.h"

void etsaia_sortu(ZAILTASUNA zailtasuna, ETSAIA babel[], JOKALARIA* jokalaria, MAILA* generatuta)
{
	if (jokalaria->pantailan_kopurua == 0)
	{
		while (generatuta->kont <= generatuta->modo) //etsaiak generatzen ditu joku moduak definitutako kantidadearen arabera
		{
			babel[generatuta->kont] = Irudia_babel(babel[generatuta->kont]);
			generatuta->kont++;
			jokalaria->pantailan_kopurua = generatuta->modo + 1;
		}
	}
}

JOKALARIA etsaiaren_mugimenduak(JOKALARIA jokalaria, ETSAIA babel[], EGOERA* egoera, BALA bala[], TIMER katanaMomentua, int arma, JOKALARIA jokalaria_ametralladora[], MAILA generatuta, BOSS boss)
{
	int i = 0;

	while ((i <= generatuta.modo) && (*egoera == JOLASTEN) && (jokalaria.pantailan_kopurua != 0))
	{

		if (babel[i].bizirik_al_dago == 1)
		{
			if (jokalaria.pos.x - 3 > babel[i].pos.x)
			{
				babel[i].pos.x += 1;
			}
			else
			{
				babel[i].pos.x -= 1;
			}

			if (jokalaria.pos.y > babel[i].pos.y)
			{
				babel[i].pos.y += 1;
			}
			else
			{
				babel[i].pos.y -= 1;
			}
			babel[i] = akabatu_babel(bala, babel[i], &jokalaria, jokalaria_ametralladora, katanaMomentua, arma, &generatuta, boss);
		}
		else
		{
			babel[i].pos.x = 1000; //bazpa
			babel[i].pos.y = 1000;
		}
		irudiaMugitu(babel[i].id, babel[i].pos.x, babel[i].pos.y);

		if (jokalaria.pos.x > babel[i].pos.x - 20 && jokalaria.pos.x <babel[i].pos.x + 25 && jokalaria.pos.y >babel[i].pos.y -20 && jokalaria.pos.y < babel[i].pos.y+ 30)
		{
			*egoera = GALDU;
		}
		i++;
	}

	return jokalaria;

}

ETSAIA akabatu_babel(BALA bala[], ETSAIA oztopoa, JOKALARIA *jokalaria, JOKALARIA jokalaria_ametralladora[], TIMER katanaMomentua, int arma, MAILA* generatuta, BOSS boss) {

	int  idAudioGame;

	if (arma != 4)
	{
		if ((bala[0].pos.x > oztopoa.pos.x - 10) && (bala[0].pos.x < oztopoa.pos.x + 20) && (bala[0].pos.y > oztopoa.pos.y - 10) && (bala[0].pos.y < oztopoa.pos.y + 40) && ((jokalaria_ametralladora[0].tiroa == 1) || (jokalaria->tiroa == 1)))
		{
			irudiaKendu(oztopoa.id);
			oztopoa.bizirik_al_dago = 0;

			idAudioGame = loadSound(BUBBLE_KILL_SOUND);
			playSound(idAudioGame);

			jokalaria->tiroa = 0;
			jokalaria_ametralladora[0].tiroa = 0;
			irudiaKendu(bala[0].id);
			jokalaria->dirua = jokalaria->dirua + 10;
			jokalaria->pantailan_kopurua--;

			if (boss.sortuta == 0)
			{
				jokalaria->babel_kills++;

			}
		}
		if ((bala[1].pos.x > oztopoa.pos.x - 10) && (bala[1].pos.x < oztopoa.pos.x + 20) && (bala[1].pos.y > oztopoa.pos.y - 10) && (bala[1].pos.y < oztopoa.pos.y + 40) && (jokalaria_ametralladora[1].tiroa == 1))
		{
			irudiaKendu(oztopoa.id);
			oztopoa.bizirik_al_dago = 0;

			idAudioGame = loadSound(BUBBLE_KILL_SOUND);
			playSound(idAudioGame);

			jokalaria_ametralladora[1].tiroa = 0;
			irudiaKendu(bala[1].id);
			jokalaria->dirua = jokalaria->dirua + 10;
			jokalaria->pantailan_kopurua--;

			if (boss.sortuta == 0)
			{
				jokalaria->babel_kills++;

			}
		}
		if ((bala[2].pos.x > oztopoa.pos.x - 10) && (bala[2].pos.x < oztopoa.pos.x + 20) && (bala[2].pos.y > oztopoa.pos.y - 10) && (bala[2].pos.y < oztopoa.pos.y + 40) && (jokalaria_ametralladora[2].tiroa == 1))
		{
			irudiaKendu(oztopoa.id);
			oztopoa.bizirik_al_dago = 0;

			idAudioGame = loadSound(BUBBLE_KILL_SOUND);
			playSound(idAudioGame);

			jokalaria_ametralladora[2].tiroa = 0;
			irudiaKendu(bala[2].id);
			jokalaria->dirua = jokalaria->dirua + 10;
			jokalaria->pantailan_kopurua--;

			if (boss.sortuta == 0)
			{
				jokalaria->babel_kills++;

			}
		}
		if ((bala[3].pos.x > oztopoa.pos.x - 10) && (bala[3].pos.x < oztopoa.pos.x + 20) && (bala[3].pos.y > oztopoa.pos.y - 10) && (bala[3].pos.y < oztopoa.pos.y + 40) && ((jokalaria_ametralladora[3].tiroa == 1) || (jokalaria->tiroa == 1)))
		{
			irudiaKendu(oztopoa.id);
			oztopoa.bizirik_al_dago = 0;

			idAudioGame = loadSound(BUBBLE_KILL_SOUND);
			playSound(idAudioGame);

			jokalaria_ametralladora[3].tiroa = 0;
			irudiaKendu(bala[3].id);
			jokalaria->dirua = jokalaria->dirua + 10;
			jokalaria->pantailan_kopurua--;

			if (boss.sortuta == 0)
			{
				jokalaria->babel_kills++;

			}
		}
		if ((bala[4].pos.x > oztopoa.pos.x - 10) && (bala[4].pos.x < oztopoa.pos.x + 20) && (bala[4].pos.y > oztopoa.pos.y - 10) && (bala[4].pos.y < oztopoa.pos.y + 40) && (jokalaria_ametralladora[4].tiroa == 1))
		{
			irudiaKendu(oztopoa.id);
			oztopoa.bizirik_al_dago = 0;

			idAudioGame = loadSound(BUBBLE_KILL_SOUND);
			playSound(idAudioGame);

			jokalaria_ametralladora[4].tiroa = 0;
			irudiaKendu(bala[4].id);
			jokalaria->dirua = jokalaria->dirua + 10;
			jokalaria->pantailan_kopurua--;

			if (boss.sortuta == 0)
			{
				jokalaria->babel_kills++;

			}
		}
		if ((bala[5].pos.x > oztopoa.pos.x - 10) && (bala[5].pos.x < oztopoa.pos.x + 20) && (bala[5].pos.y > oztopoa.pos.y - 10) && (bala[5].pos.y < oztopoa.pos.y + 40) && (jokalaria_ametralladora[5].tiroa == 1))
		{
			irudiaKendu(oztopoa.id);
			oztopoa.bizirik_al_dago = 0;

			idAudioGame = loadSound(BUBBLE_KILL_SOUND);
			playSound(idAudioGame);

			irudiaKendu(bala[5].id);
			jokalaria_ametralladora[5].tiroa = 0;
			jokalaria->dirua = jokalaria->dirua + 10;
			jokalaria->pantailan_kopurua--;

			if (boss.sortuta == 0)
			{
				jokalaria->babel_kills++;

			}
		}
		if ((bala[6].pos.x > oztopoa.pos.x - 10) && (bala[6].pos.x < oztopoa.pos.x + 20) && (bala[6].pos.y > oztopoa.pos.y - 10) && (bala[6].pos.y < oztopoa.pos.y + 40) && (jokalaria_ametralladora[6].tiroa == 1))
		{
			irudiaKendu(oztopoa.id);
			oztopoa.bizirik_al_dago = 0;

			idAudioGame = loadSound(BUBBLE_KILL_SOUND);
			playSound(idAudioGame);

			irudiaKendu(bala[6].id);
			jokalaria_ametralladora[6].tiroa = 0;
			jokalaria->dirua = jokalaria->dirua + 10;
			jokalaria->pantailan_kopurua--;
			jokalaria->kont = 0;

			if (boss.sortuta == 0)
			{
				jokalaria->babel_kills++;

			}
		}
	}
	if ((arma == 4) && (katanaMomentua.pos.x > oztopoa.pos.x - 50 && katanaMomentua.pos.x <oztopoa.pos.x + 80 && katanaMomentua.pos.y >oztopoa.pos.y - 40 && katanaMomentua.pos.y < oztopoa.pos.y + 90 && katanaMomentua.tartea == 1))
	{
		irudiaKendu(oztopoa.id);
		oztopoa.bizirik_al_dago = 0;

		jokalaria->pantailan_kopurua--;
		jokalaria->dirua = jokalaria->dirua + 10;

		if (boss.sortuta == 0)
		{
			jokalaria->babel_kills++;
		}
	}
	if (boss.hilda == 1)
	{
		oztopoa.bizirik_al_dago == 0;
		irudiaKendu(oztopoa.id);
		jokalaria->pantailan_kopurua--;

	}

	return oztopoa;
}
BOSS akabatu_boss(BALA bala[], BOSS boss, JOKALARIA *jokalaria, JOKALARIA jokalaria_ametralladora[], TIMER katanaMomentua, int arma, MAILA generatuta, ZAILTASUNA zailtasuna, BALA bala_boss[])
{
	int  idAudioGame;

	if (boss.sortuta == 1)
	{
		if (arma != 4)
		{
			if ((bala[0].pos.x > boss.pos.x - 10) && (bala[0].pos.x < boss.pos.x + 120) && (bala[0].pos.y > boss.pos.y - 10) && (bala[0].pos.y < boss.pos.y + 80) && ((jokalaria_ametralladora[0].tiroa == 1) || (jokalaria->tiroa == 1)))
			{
				idAudioGame = loadSound(BUBBLE_KILL_SOUND);
				playSound(idAudioGame);

				boss.jasotako_tiroak++;
				irudiaKendu(bala[0].id);
				bala[0].pos.x = 1000;
				bala[0].pos.y = 1000;

				jokalaria->tiroa = 0;
			}
			if ((bala[1].pos.x > boss.pos.x - 10) && (bala[1].pos.x < boss.pos.x + 120) && (bala[1].pos.y > boss.pos.y - 10) && (bala[1].pos.y < boss.pos.y + 80) && (jokalaria_ametralladora[1].tiroa == 1))
			{
				idAudioGame = loadSound(BUBBLE_KILL_SOUND);
				playSound(idAudioGame);

				boss.jasotako_tiroak++;
				bala[1].pos.x = 1000;
				bala[1].pos.y = 1000;
			}
			if ((bala[2].pos.x > boss.pos.x - 10) && (bala[2].pos.x < boss.pos.x + 120) && (bala[2].pos.y > boss.pos.y - 10) && (bala[2].pos.y < boss.pos.y + 80) && (jokalaria_ametralladora[2].tiroa == 1))
			{
				idAudioGame = loadSound(BUBBLE_KILL_SOUND);
				playSound(idAudioGame);

				boss.jasotako_tiroak++;
				bala[2].pos.x = 1000;
				bala[2].pos.y = 1000;
			}
			if ((bala[3].pos.x > boss.pos.x - 10) && (bala[3].pos.x < boss.pos.x + 120) && (bala[3].pos.y > boss.pos.y - 10) && (bala[3].pos.y < boss.pos.y + 80) && ((jokalaria_ametralladora[3].tiroa == 1) || (jokalaria->tiroa == 1)))
			{
				idAudioGame = loadSound(BUBBLE_KILL_SOUND);
				playSound(idAudioGame);

				boss.jasotako_tiroak++;
				bala[3].pos.x = 1000;
				bala[3].pos.y = 1000;

			}
			if ((bala[4].pos.x > boss.pos.x - 10) && (bala[4].pos.x < boss.pos.x + 120) && (bala[4].pos.y > boss.pos.y - 10) && (bala[4].pos.y < boss.pos.y + 80) && (jokalaria_ametralladora[4].tiroa == 1))
			{
				idAudioGame = loadSound(BUBBLE_KILL_SOUND);
				playSound(idAudioGame);

				boss.jasotako_tiroak++;
				bala[4].pos.x = 1000;
				bala[4].pos.y = 1000;
			}
			if ((bala[5].pos.x > boss.pos.x - 10) && (bala[5].pos.x < boss.pos.x + 120) && (bala[5].pos.y > boss.pos.y - 10) && (bala[5].pos.y < boss.pos.y + 80) && (jokalaria_ametralladora[5].tiroa == 1))
			{
				idAudioGame = loadSound(BUBBLE_KILL_SOUND);
				playSound(idAudioGame);

				boss.jasotako_tiroak++;
				bala[5].pos.x = 1000;
				bala[5].pos.y = 1000;
			}
			if ((bala[6].pos.x > boss.pos.x - 10) && (bala[6].pos.x < boss.pos.x + 120) && (bala[6].pos.y > boss.pos.y - 10) && (bala[6].pos.y < boss.pos.y + 80) && (jokalaria_ametralladora[6].tiroa == 1))
			{
				idAudioGame = loadSound(BUBBLE_KILL_SOUND);
				playSound(idAudioGame);

				boss.jasotako_tiroak++;
				bala[6].pos.x = 1000;
				bala[6].pos.y = 1000;
			}
		}
		if ((arma == 4) && (katanaMomentua.pos.x > boss.pos.x - 50 && katanaMomentua.pos.x <boss.pos.x + 80 && katanaMomentua.pos.y >boss.pos.y - 40 && katanaMomentua.pos.y < boss.pos.y + 90 && katanaMomentua.tartea == 1))
		{
			boss.jasotako_tiroak++;
		}

		if (boss.jasotako_tiroak >= 30)
		{
			boss.sortuta = 0;
			boss.hilda = 1;
			boss.jasotako_tiroak = 0;

			if (zailtasuna == APOCALYPTIC)
			{
				boss.beharrezko_killak += 80; //berriro agertu dadin
			}
			else
			{
				boss.beharrezko_killak += 40;
			}

			jokalaria->dirua += 50;
			jokalaria->babel_kills += generatuta.modo + 1;

			irudiaKendu(bala_boss[0].id, bala_boss[0].pos.x, bala_boss[0].pos.y);
			irudiaKendu(bala_boss[1].id, bala_boss[0].pos.x, bala_boss[1].pos.y);
			irudiaKendu(bala_boss[2].id, bala_boss[0].pos.x, bala_boss[2].pos.y);
			irudiaKendu(bala_boss[3].id, bala_boss[0].pos.x, bala_boss[3].pos.y);
			irudiaKendu(boss.id);
		}
	}
	return boss;
}
void boss_bala_generazioa(BOSS boss, BALA bala_boss[])
{
	int i = 0;

	for (i; i <= 3; i++)
	{
		bala_boss[i].id = Irudia_bala_boss(boss);
	}
}
void boss_tiroa(BOSS boss, BALA bala_boss[], EGOERA* egoera, JOKALARIA jokalaria)
{

	if ((boss.sortuta == 1))
	{

		if ((bala_boss[0].pos.x > -10) && (bala_boss[0].pos.x < 650))
		{
			bala_boss[0].pos.x += 2;
			irudiaMugitu(bala_boss[0].id, bala_boss[0].pos.x, bala_boss[0].pos.y);

		}
		else
		{
			bala_boss[0].pos.x = boss.pos.x + 50;
			bala_boss[0].pos.y = boss.pos.y + 50;
		}

		if ((bala_boss[1].pos.x > -10) && (bala_boss[1].pos.x < 650))
		{
			bala_boss[1].pos.x -= 2;
			irudiaMugitu(bala_boss[1].id, bala_boss[1].pos.x, bala_boss[1].pos.y);

		}
		else
		{
			bala_boss[1].pos.x = boss.pos.x + 50;
			bala_boss[1].pos.y = boss.pos.y + 50;
		}

		if ((bala_boss[2].pos.y > -10) && (bala_boss[2].pos.y < 480))
		{
			bala_boss[2].pos.y += 2;
			irudiaMugitu(bala_boss[2].id, bala_boss[2].pos.x, bala_boss[2].pos.y);

		}
		else
		{
			bala_boss[2].pos.x = boss.pos.x + 50;
			bala_boss[2].pos.y = boss.pos.y + 50;
		}

		if ((bala_boss[3].pos.y > -10) && (bala_boss[3].pos.y < 480))
		{
			bala_boss[3].pos.y -= 2;
			irudiaMugitu(bala_boss[3].id, bala_boss[3].pos.x, bala_boss[3].pos.y);
		}
		else
		{
			bala_boss[3].pos.x = boss.pos.x + 50;
			bala_boss[3].pos.y = boss.pos.y + 50;
		}

		if ((bala_boss[0].pos.x > jokalaria.pos.x - 0) && (bala_boss[0].pos.x < jokalaria.pos.x + 20) && (bala_boss[0].pos.y > jokalaria.pos.y - 0) && (bala_boss[0].pos.y < jokalaria.pos.y + 20))
		{
			*egoera = GALDU;
		}
		if ((bala_boss[1].pos.x > jokalaria.pos.x - 0) && (bala_boss[1].pos.x < jokalaria.pos.x + 20) && (bala_boss[1].pos.y > jokalaria.pos.y - 0) && (bala_boss[1].pos.y < jokalaria.pos.y + 20))
		{
			*egoera = GALDU;
		}
		if ((bala_boss[2].pos.x > jokalaria.pos.x - 0) && (bala_boss[2].pos.x < jokalaria.pos.x + 20) && (bala_boss[2].pos.y > jokalaria.pos.y - 0) && (bala_boss[2].pos.y < jokalaria.pos.y + 20))
		{
			*egoera = GALDU;
		}
		if ((bala_boss[3].pos.x > jokalaria.pos.x - 0) && (bala_boss[3].pos.x < jokalaria.pos.x + 20) && (bala_boss[3].pos.y > jokalaria.pos.y - 0) && (bala_boss[3].pos.y < jokalaria.pos.y + 20))
		{
			*egoera = GALDU;
		}
	}
}

BOSS boss_mugimendua(BOSS boss, ZAILTASUNA zailtasuna, JOKALARIA jokalaria, EGOERA* egoera)
{
	if (boss.sortuta == 1)
	{
		if (boss.beharrezko_killak == 30) //jokalaria.kills-ek kontrolatzen du noiz sortu bossa, ez dugulako nahi nibel hasieran agertzea
		{
			if (boss.pos.x <= 0)
			{
				boss.movx = 1;
			}
			if (boss.pos.x >= 520)
			{
				boss.movx = 0;
			}

			if (boss.movx == 1)
			{
				boss.pos.x = boss.pos.x + 1;
			}
			if (boss.movx == 0)
			{
				boss.pos.x = boss.pos.x - 1;
			}
		}
		if (boss.beharrezko_killak == 70)
		{
			if (boss.pos.y <= 0)
			{
				boss.movy = 1;
			}
			if (boss.pos.y >= 400)
			{
				boss.movy = 0;
			}

			if (boss.movy == 1)
			{
				boss.pos.y = boss.pos.y + 1;
			}
			if (boss.movy == 0)
			{
				boss.pos.y = boss.pos.y - 1;
			}
		}
		if (boss.beharrezko_killak == 110)
		{
			if (boss.pos.y <= 0)
			{
				boss.movy = 1;
			}
			if (boss.pos.y >= 400)
			{
				boss.movy = 0;
			}

			if (boss.movy == 1)
			{
				boss.pos.y = boss.pos.y + 1;
			}
			if (boss.movy == 0)
			{
				boss.pos.y = boss.pos.y - 1;
			}
		}
		if (boss.beharrezko_killak >= 150)
		{
			if (boss.pos.x <= 0)
			{
				boss.movx = 1;
			}
			if (boss.pos.x >= 520)
			{
				boss.movx = 0;
			}

			if (boss.pos.y <= 0)
			{
				boss.movy = 1;
			}
			if (boss.pos.y >= 400)
			{
				boss.movy = 0;
			}

			if (boss.movx == 1)
			{
				boss.pos.x = boss.pos.x + 1;
			}
			if (boss.movx == 0)
			{
				boss.pos.x = boss.pos.x - 1;
			}

			if (boss.movy == 1)
			{
				boss.pos.y = boss.pos.y + 2;
			}
			if (boss.movy == 0)
			{
				boss.pos.y = boss.pos.y - 2;
			}
		}
		if ((boss.pos.x > jokalaria.pos.x - 100) && (boss.pos.x < jokalaria.pos.x + 0) && (boss.pos.y > jokalaria.pos.y - 80) && (boss.pos.y < jokalaria.pos.y + 20))
		{
			*egoera = GALDU;
		}
	}

	return boss;
}

int Irudia_bala_boss(BOSS boss)
{
	int balaId;

	balaId = -1;
	balaId = irudiaKargatu(BALA_ARMA);
	irudiaMugitu(balaId, boss.pos.x + 50, boss.pos.y + 50);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return balaId;
}

BOSS Irudia_boss(BOSS boss, JOKALARIA jokalaria, ZAILTASUNA zailtasuna)
{
	boss.id = -1;

	if (zailtasuna == EASY)
	{
		boss.id = irudiaKargatu(BOSS1);
	}
	if (zailtasuna == MEDIUM)
	{
		boss.id = irudiaKargatu(BOSS2);
	}
	if (zailtasuna == HARD)
	{
		boss.id = irudiaKargatu(BOSS2);
	}
	if (zailtasuna == APOCALYPTIC)
	{
		boss.id = irudiaKargatu(BOSS2);
	}

	if (jokalaria.pos.x < 320)
	{
		boss.pos.x = 520;
	}
	else
	{
		boss.pos.x = 0;
	}
	if (jokalaria.pos.y < 240)
	{
		boss.pos.y = 0;
	}
	else
	{
		boss.pos.y = 400;
	}

	irudiaMugitu(boss.id, boss.pos.x, boss.pos.y);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return boss;
}

ETSAIA Irudia_babel(ETSAIA babel)
{
	int posizioa;

	posizioa = rand() % 4;

	babel.id = -1;

	if (posizioa == 0)//ezkerreko ertza
	{
		babel.pos.x = rand() % 40;
		babel.pos.y = rand() % 480;
		//generatuta->apocaliptic = 1;
		babel.bizirik_al_dago = 1;
	}
	else if (posizioa == 1)//beheko ertza
	{
		babel.pos.x = rand() % 640;
		do
		{
			babel.pos.y = rand() % 480;
		} while (babel.pos.y < 440);

		//generatuta->apocaliptic = 1;
		babel.bizirik_al_dago = 1;
	}
	else if (posizioa == 2)//Goiko ertza
	{
		babel.pos.y = rand() % 40;
		babel.pos.x = rand() % 640;

		//generatuta->apocaliptic = 1;
		babel.bizirik_al_dago = 1;
	}
	else if (posizioa == 3)//eskumako ertza
	{
		babel.pos.y = rand() % 480;
		do
		{
			babel.pos.x = rand() % 640;
		} while (babel.pos.x < 600);

		//generatuta->apocaliptic = 1;
		babel.bizirik_al_dago = 1;
	}

	babel.id = irudiaKargatu(BABEL);

	irudiaMugitu(babel.id, babel.pos.x, babel.pos.y);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
	return babel;
}